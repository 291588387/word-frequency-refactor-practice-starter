import java.util.*;

public class WordFrequencyGame {
    public String getResult(String inputString) {
        try {
            String splitRegex = "\\s+";
            String[] splitString = inputString.split(splitRegex);
            Map<String, Integer> wordFrequencyMap = countWordFrequency(splitString);
            return generateResult(wordFrequencyMap);
        } catch (NullPointerException exception) {
            return "Calculate Error";
        }
    }

    private String generateResult(Map<String, Integer> wordFrequencyMap) {
        String lineBreaker = "\n";
        StringJoiner joiner = new StringJoiner(lineBreaker);
        wordFrequencyMap.entrySet().stream()
                .sorted((currentEntry, nextEntry) -> nextEntry.getValue().compareTo(currentEntry.getValue()))
                .forEach(entry -> joiner.add(entry.getKey() + " " + entry.getValue()));
        return joiner.toString();
    }

    private Map<String, Integer> countWordFrequency(String[] splitString) {
        int initialWordCount = 0;
        int singleWordCount = 1;
        Map<String, Integer> wordFrequencyMap = new HashMap<>();
        Arrays.stream(splitString)
                .forEach(string -> {
                    wordFrequencyMap.put(string, wordFrequencyMap.getOrDefault(string, initialWordCount) + singleWordCount);
                });
        return wordFrequencyMap;
    }
}

# ORID
## O:

- Held our first presentation, demonstrated about Strategy Pattern. Containing Definition, Reason for selection, Usage scenario, Strategy pattern demo, Pros & Cons.
- Learned about Refactoring, including when to refactor, and to pay attention to test protection, code smell, refactor techs etc.
- Practiced refactoring with word frequency game example, following the code smell, refactor and small step commit procedure.
## R:

- I felt fruitful and pleasant
## I:
- It's really delighting to convey our first presentation successfully and got positive feedback.
- I benefited a lot from refactoring tutorial since our teachers provided good examples and led us to refactoring step by step.
## D:
- Take the initiative to distribute tasks and obtain more background knowledges for the next presentation.
- Keep on refactoring what needs to be perfected in my codes.